import Vue from 'vue'
import App from './App.vue'

// Bootstrap
import {BootstrapVue} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// FontAwesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {faMicrophone, faRobot, faStop, faTimes} from '@fortawesome/free-solid-svg-icons'

Vue.use(BootstrapVue)

library.add(faMicrophone, faTimes, faRobot, faStop)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')
