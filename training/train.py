#!/usr/bin/env python
# coding: utf-8
# 3rd Party libraries

import copy
import os
from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import nemo.collections.asr as nemo_asr
import pytorch_lightning as pl
import torch
import torch.nn as nn
from nemo.utils import exp_manager, logging
from omegaconf import OmegaConf, open_dict

from s2t import TrainConfig, settings


def main(
    model_type: str,
    source_config: list[TrainConfig],
    target_config: TrainConfig,
    tokenizer: str,
):
    base_model = model_type.split("_")[0]

    if base_model == "quartznet":
        from s2t.models.nemo.quartznet import quartznet as model_fn
    elif base_model == "jasper":
        from s2t.models.nemo.jasper import jasper as model_fn
    elif base_model == "citrinet":
        from s2t.models.nemo.citrinet import citrinet as model_fn
    elif base_model == "conformerCTC":
        from s2t.models.nemo.conformerCTC import conformer_CTC as model_fn
    elif base_model == "conformerTD":
        from s2t.models.nemo.conformerTD import conformer_TD as model_fn
    elif base_model == "contextnet":
        from s2t.models.nemo.contextnet import contextnet as model_fn
    else:
        raise ValueError("There is currently no base model of this type implemented!")

    model_fn(model_type, source_config, target_config, tokenizer)


def parse_args():
    parser = ArgumentParser()

    parser.add_argument(
        "--model_type",
        "-m",
        help="The model",
        required=True,
        choices=settings.BASE_MODEL_MAP.keys(),
    )

    parser.add_argument(
        "--source_config",
        "-s",
        help="The source config, joined with comma",
        required=True,
    )

    parser.add_argument(
        "--target_config",
        "-t",
        help="The target dataset. Use any available e.g. 'CH_meteo",
        required=True,
    )

    parser.add_argument(
        "--tokenizer",
        help="Choose which tokenizer to use: source or target",
        choices=["source", "target"],
        required=True,
    )

    parser.add_argument(
        "--test",
        help="Only prepare everything, do not actually train the model.",
        default=False,
        action="store_true",
    )

    return parser.parse_args()


def _split_config(config_str) -> list[TrainConfig]:
    """

    The format must be:
    dataset-True,dataset-False,dataset-True,...

    Args:
        config_str:

    Returns:

    """
    parts = config_str.split(",")

    configs: list[TrainConfig] = []

    for part in parts:
        if part in ("en", "de"):
            configs.append(TrainConfig(part, None))
        else:
            dataset, full_retrain = part.split("-")
            configs.append(
                TrainConfig(dataset, True if full_retrain == "True" else False)
            )

    return configs


if __name__ == "__main__":
    args = parse_args()

    if args.test:
        settings.IS_TEST_RUN = True

    main(
        model_type=args.model_type,
        source_config=_split_config(args.source_config),
        target_config=_split_config(args.target_config)[0],
        tokenizer=args.tokenizer,
    )
