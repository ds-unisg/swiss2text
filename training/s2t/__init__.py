#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dataclasses import dataclass
from typing import Optional


@dataclass
class TrainConfig:
    dataset: str
    full_retrain: Optional[bool]

    def __str__(self):
        if self.full_retrain is None:
            return f"{self.dataset}"
        else:
            return f"{self.dataset}-{self.full_retrain}"
