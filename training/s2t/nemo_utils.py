from pathlib import Path

import nemo.collections.asr as nemo_asr
import numpy as np
import torch
from nemo.core.classes import IterableDataset
from nemo.core.neural_types import AudioSignal, LengthsType, NeuralType
from torch.utils.data import DataLoader

SAMPLE_RATE = 16_000


def list_models(path: Path) -> list[Path]:
    return list(path.glob("*.nemo"))


def load_model(model_name: str) -> nemo_asr.models.ASRModel:
    if model_name.endswith(".nemo"):
        load_fn = nemo_asr.models.ASRModel.restore_from
    else:
        load_fn = nemo_asr.models.ASRModel.from_pretrained
    cfg = load_fn(model_name, return_config=True)
    cfg.train_ds, cfg.validation_ds, cfg.test_ds = None, None, None

    model = load_fn(
        model_name, override_config_path=cfg, map_location=torch.device("cpu")
    )
    model.eval()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    return model


# simple data layer to pass audio signal
class AudioDataLayer(IterableDataset):
    def __init__(self, sample_rate):
        super().__init__()
        self._sample_rate = sample_rate
        self.output = True

    @property
    def output_types(self):
        return {
            "audio_signal": NeuralType(("B", "T"), AudioSignal(freq=self._sample_rate)),
            "a_sig_length": NeuralType(tuple("B"), LengthsType()),
        }

    def __iter__(self):
        return self

    def __next__(self):
        if not self.output:
            raise StopIteration
        self.output = False
        return torch.as_tensor(self.signal, dtype=torch.float32), torch.as_tensor(
            self.signal_shape, dtype=torch.int64
        )

    def set_signal(self, signal):
        self.signal = signal.astype(np.float32) / 32768.0
        self.signal_shape = self.signal.size
        self.output = True

    def __len__(self):
        return 1


_data_layer = AudioDataLayer(sample_rate=SAMPLE_RATE)
_data_loader = DataLoader(_data_layer, batch_size=1, collate_fn=_data_layer.collate_fn)


# inference method for audio signal (single instance)
def infer_signal(model, signal):
    _data_layer.set_signal(signal)
    batch = next(iter(_data_loader))
    audio_signal, audio_signal_len = batch
    audio_signal, audio_signal_len = audio_signal.to(model.device), audio_signal_len.to(
        model.device
    )
    log_probs, encoded_len, predictions = model.forward(
        input_signal=audio_signal, input_signal_length=audio_signal_len
    )
    return log_probs


class FrameASR:
    def __init__(
        self, model, model_definition, frame_len=2, frame_overlap=2.5, offset=10
    ):
        """
        Args:
          frame_len: frame's duration, seconds
          frame_overlap: duration of overlaps before and after current frame, seconds
          offset: number of symbols to drop for smooth streaming
        """
        self.model = model
        self.vocab = list(model_definition["labels"])
        self.vocab.append("_")

        self.sr = model_definition["sample_rate"]
        self.frame_len = frame_len
        self.n_frame_len = int(frame_len * self.sr)
        self.frame_overlap = frame_overlap
        self.n_frame_overlap = int(frame_overlap * self.sr)
        timestep_duration = model_definition["AudioToMelSpectrogramPreprocessor"][
            "window_stride"
        ]
        for block in model_definition["JasperEncoder"]["jasper"]:
            timestep_duration *= block["stride"][0] ** block["repeat"]
        self.n_timesteps_overlap = int(frame_overlap / timestep_duration) - 2
        self.buffer = np.zeros(
            shape=2 * self.n_frame_overlap + self.n_frame_len, dtype=np.float32
        )
        self.offset = offset
        self.reset()

    def _decode(self, frame, offset=0):
        assert len(frame) == self.n_frame_len
        self.buffer[: -self.n_frame_len] = self.buffer[self.n_frame_len :]
        self.buffer[-self.n_frame_len :] = frame
        logits = infer_signal(self.model, self.buffer).cpu().numpy()[0]
        decoded = self._greedy_decoder(
            logits[self.n_timesteps_overlap : -self.n_timesteps_overlap], self.vocab
        )
        return decoded[: len(decoded) - offset]

    @torch.no_grad()
    def transcribe(self, frame=None, merge=True):
        if frame is None:
            frame = np.zeros(shape=self.n_frame_len, dtype=np.float32)
        if len(frame) < self.n_frame_len:
            frame = np.pad(frame, [0, self.n_frame_len - len(frame)], "constant")
        unmerged = self._decode(frame, self.offset)
        if not merge:
            return unmerged
        return self.greedy_merge(unmerged)

    def reset(self):
        """
        Reset frame_history and decoder's state
        """
        self.buffer = np.zeros(shape=self.buffer.shape, dtype=np.float32)
        self.prev_char = ""

    @staticmethod
    def _greedy_decoder(logits, vocab):
        s = ""
        for i in range(logits.shape[0]):
            s += vocab[np.argmax(logits[i])]
        return s

    def greedy_merge(self, s):
        s_merged = ""

        for i in range(len(s)):
            if s[i] != self.prev_char:
                self.prev_char = s[i]
                if self.prev_char != "_":
                    s_merged += self.prev_char
        return s_merged
