from pathlib import Path

# Namings
EXPERIMENT_PROJECT = "swiss2text"
TOKENIZER_NAME = "tokenizer_spe_bpe_v1024"
ANNOTATION_FILE = "transcriptions_{}.json"

# Model types
BASE_MODEL_MAP = {
    # Conformer-CTC models EN
    "conformerCTC_Large": "stt_en_conformer_ctc_large",
    "conformerCTC_LargeLS": "stt_en_conformer_ctc_large_ls",
    "conformerCTC_Medium": "stt_en_conformer_ctc_medium",
    "conformerCTC_MediumLS": "stt_en_conformer_ctc_medium_ls",
    "conformerCTC_Small": "stt_en_conformer_ctc_small",
    "conformerCTC_SmallLS": "stt_en_conformer_ctc_small_ls",
    # Conformer Transducer Models EN
    "conformerTD_Large": "stt_en_conformer_transducer_large",
    "conformerTD_Large_DE": "stt_de_conformer_transducer_large",
    "conformerTD_Medium": "stt_en_conformer_transducer_medium",
    "conformerTD_Small": "stt_en_conformer_transducer_small",
    # Contextnet Models EN
    "contextnet_Base": "stt_en_contextnet_1024",
    "contextnet_Mls": "stt_en_contextnet_1024_mls",
    # Quartznet Models, EN and DE
    "quartznet_Base": "stt_en_quartznet15x5",
    "quartznet_DE": "stt_de_quartznet15x5",
    # Jasper Model
    "jasper_Base": "stt_en_jasper10x5dr",
    # Citrinet Models, EN and DE
    "citrinet_256": "stt_en_citrinet_256",
    "citrinet_256gamma": "stt_en_citrinet_256_gamma_0_25",
    "citrinet_512": "stt_en_citrinet_512",
    "citrinet_512gamma": "stt_en_citrinet_512_gamma_0_25",
    "citrinet_1024": "stt_en_citrinet_1024",
    "citrinet_1024gamma": "stt_en_citrinet_1024_gamma_0_25",
    "citrinet_DE": "stt_de_citrinet_1024",
}

# Directories
MAIN_DATA_DIR = Path("/data")
MODEL_DIR = MAIN_DATA_DIR.joinpath("models")
DATA_DIR = MAIN_DATA_DIR.joinpath("voice")

# Arguments
IS_TEST_RUN = False

# General training parameters
NB_EPOCHS = 256
TRAIN_BATCHSIZE = 8
TRAIN_NUMWORKERS = 4
TEST_BATCHSIZE = 16
TEST_NUMWORKERS = 4
VAL_BATCHSIZE = 16
VAL_NUMWORKERS = 4
