import copy
from dataclasses import dataclass
from typing import Optional

import nemo.collections.asr as nemo_asr
import pytorch_lightning as pl
import torch
import torch.nn as nn
from nemo.utils import exp_manager, logging
from omegaconf import OmegaConf, open_dict

from s2t import TrainConfig, settings


def citrinet_enable_bn_se(m):
    layer_type = type(m)
    if layer_type == nn.BatchNorm1d or "SqueezeExcite" in layer_type.__name__:
        m.train()
        for param in m.parameters():
            param.requires_grad_(True)


def citrinet(
    model_type: str,
    source_config: list[TrainConfig],
    target_config: TrainConfig,
    tokenizer: str,
):

    source_config_str = ",".join(map(str, source_config))
    target_config_str = f"{target_config}"

    experiment_name = f"{model_type}-{source_config_str}-TO-{target_config_str}"

    target_voice_dir = settings.DATA_DIR.joinpath(target_config.dataset)

    if tokenizer == "source":
        tokenizer_dir = (
            settings.DATA_DIR.joinpath(source_config[-1].dataset)
            .joinpath("tokenizers")
            .joinpath(settings.TOKENIZER_NAME)
        )
    elif tokenizer == "target":
        tokenizer_dir = target_voice_dir.joinpath("tokenizers").joinpath(
            settings.TOKENIZER_NAME
        )
    else:
        raise ValueError(f"Illegal tokenizer value: {tokenizer}")

    if not tokenizer_dir.exists():
        raise ValueError("The tokenizer for this dataset does not exist!")

    train_manifest = target_voice_dir.joinpath(settings.ANNOTATION_FILE.format("train"))
    dev_manifest = target_voice_dir.joinpath(settings.ANNOTATION_FILE.format("val"))
    test_manifest = target_voice_dir.joinpath(settings.ANNOTATION_FILE.format("test"))

    if source_config[-1].dataset == "en":
        model = nemo_asr.models.ASRModel.from_pretrained(
            settings.BASE_MODEL_MAP[model_type], map_location=torch.device("cpu")
        )
    else:
        source_model_file = settings.MODEL_DIR.joinpath(
            f"{model_type}-{source_config_str}.nemo"
        )
        model = nemo_asr.models.ASRModel.restore_from(
            f"{source_model_file}", map_location=torch.device("cpu")
        )

    model.change_vocabulary(
        new_tokenizer_dir=f"{tokenizer_dir}", new_tokenizer_type="bpe"
    )

    if target_config.full_retrain:
        model.encoder.unfreeze()
        logging.info("Model encoder has been un-frozen")
    else:
        model.encoder.freeze()
        model.encoder.apply(citrinet_enable_bn_se)
        logging.info("Model encoder has been frozen")

    cfg = copy.deepcopy(model.cfg)

    # Setup new tokenizer
    cfg.tokenizer.dir = f"{tokenizer_dir}"
    cfg.tokenizer.type = "bpe"

    # Set tokenizer config
    model.cfg.tokenizer = cfg.tokenizer

    # Setup train, validation, test configs
    with open_dict(cfg):
        # Train dataset
        cfg.train_ds.manifest_filepath = f"{train_manifest}"
        cfg.train_ds.batch_size = settings.TRAIN_BATCHSIZE
        cfg.train_ds.num_workers = settings.TRAIN_NUMWORKERS
        cfg.train_ds.pin_memory = True
        cfg.train_ds.use_start_end_token = True
        cfg.train_ds.trim_silence = True

        # Validation dataset
        cfg.validation_ds.manifest_filepath = f"{dev_manifest}"
        cfg.validation_ds.batch_size = settings.VAL_BATCHSIZE
        cfg.validation_ds.num_workers = settings.VAL_NUMWORKERS
        cfg.validation_ds.pin_memory = True
        cfg.validation_ds.use_start_end_token = True
        cfg.validation_ds.trim_silence = True

        # Test dataset
        cfg.test_ds.manifest_filepath = f"{test_manifest}"
        cfg.test_ds.batch_size = settings.TEST_BATCHSIZE
        cfg.test_ds.num_workers = settings.TEST_NUMWORKERS
        cfg.test_ds.pin_memory = True
        cfg.test_ds.use_start_end_token = True
        cfg.test_ds.trim_silence = True

    # Setup Data
    model.setup_training_data(cfg.train_ds)
    model.setup_validation_data(cfg.validation_ds)
    model.setup_test_data(cfg.test_ds)

    # Setup Optimizer
    with open_dict(model.cfg.optim):
        model.cfg.optim.name = "novograd"
        model.cfg.optim.lr = 0.025
        model.cfg.optim.weight_decay = 0.001
        model.cfg.optim.sched.warmup_steps = (
            None  # Remove default number of steps of warmup
        )
        model.cfg.optim.sched.warmup_ratio = 0.10  # 10 % warmup
        model.cfg.optim.sched.min_lr = 1e-9

    # Setup Augmentation
    with open_dict(model.cfg.spec_augment):
        model.cfg.spec_augment.freq_masks = 2
        model.cfg.spec_augment.freq_width = 25
        model.cfg.spec_augment.time_masks = 10
        model.cfg.spec_augment.time_width = 0.05

    model.spec_augmentation = model.from_config_dict(model.cfg.spec_augment)

    model._wer.log_prediction = True

    if torch.cuda.is_available():
        gpus = -1
        strategy = "ddp"
    else:
        gpus = 0
        strategy = None

    trainer = pl.Trainer(
        gpus=gpus,
        max_epochs=settings.NB_EPOCHS,
        accumulate_grad_batches=1,
        checkpoint_callback=False,
        logger=False,
        log_every_n_steps=5,
        check_val_every_n_epoch=10,
        strategy=strategy,
    )

    # Setup model with the trainer
    model.set_trainer(trainer)

    # finally, update the model's internal config
    model.cfg = model._cfg

    config = exp_manager.ExpManagerConfig(
        exp_dir=f"experiments/",
        name=experiment_name,
        checkpoint_callback_params=exp_manager.CallbackParams(
            monitor="val_wer",
            mode="min",
            always_save_nemo=True,
            save_best_model=True,
        ),
        create_tensorboard_logger=False,
        create_wandb_logger=True,
        wandb_logger_kwargs={
            "name": experiment_name,
            "project": settings.EXPERIMENT_PROJECT,
        },
    )

    config = OmegaConf.structured(config)
    _ = exp_manager.exp_manager(trainer, config)

    logging.info("Will now start training")
    if not settings.IS_TEST_RUN:
        trainer.fit(model)

    model_save_path = settings.MODEL_DIR.joinpath(
        f"{model_type}-{source_config_str},{target_config_str}.nemo"
    )
    logging.info(f"Will save trained model to '{model_save_path}")
    if not settings.IS_TEST_RUN:
        model.save_to(f"{model_save_path}")
