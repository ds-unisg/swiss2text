#!/usr/bin/env bash
set -euo pipefail

: "${LOCAL_CODE:?You must set your code directory.}"
: "${LOCAL_DATA:?You must set your data directory.}"

: "${DATASET:?Must set DATASET}"
: "${VOCAB_SIZE:?Must set VOCAB_SIZE}"

: "${TYPE_TOKENIZER:?Must set TYPE_TOKENIZER (--tokenizer)}"
: "${TYPE_SPE:?Must set TYPE_SPE (--spe_type)}"

export GPUS="none"
export MEMORY_LIMIT="16"
export LOCAL_CODE="${LOCAL_CODE}"
export LOCAL_DATA="${LOCAL_DATA}"

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

"${script_dir}/start.sh" \
  python /home/user/NeMo/scripts/tokenizers/process_asr_text_tokenizer.py \
    --manifest="/data/voice/${DATASET}/transcriptions_train.json" \
    --vocab_size="${VOCAB_SIZE}" \
    --data_root="/data/voice/${DATASET}/tokenizers" \
    --tokenizer="${TYPE_TOKENIZER}" \
    --spe_type="${TYPE_SPE}" \
    --spe_character_coverage=1.0 \
    --no_lower_case \
    --log
