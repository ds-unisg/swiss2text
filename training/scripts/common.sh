# You shouldn't need to modify anything later than here.
repo="registry.gitlab.com/ds-unisg/servers/python-docker"

python="39"
pytorch="110"
nemo="17"

if [ "${GPUS}" == "none" ];
then
  arch="cpu"
else
  arch="gpu"
fi

export NEMO_IMAGE="${repo}/py${python}/${arch}-ds-pytorch${pytorch}-nemo${nemo}"

return 0
