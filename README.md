# Swiss2Text
A project for Automatic Speech Recognition from Spoken Swiss German to Text German.

## Folder Structure

- `demo`: Contains a few wav files for easy demonstration
- `swiss2text-api`: API for the Web Interface
- `swiss2text-web`: The web Interface
- `docker-compose.yml`: Purely for Deploying API and Web
- `Makefile`: Building and deploying API and Web
- `training`: Main entrypoint for training the models
- `notebooks`: Experimental jupyter notebooks (not working reliably)


## Training
Currently, the main target models are NVIDIA's [NeMo](https://github.com/NVIDIA/NeMo).

The main entrypoint into training is the `training/nemo-train.py` script.

## Setup
All development is currently done in Python 3.9.7 (any 3.9 should work) and on Linux.

Look at `training/scripts/run.sh` how to run your local docker image. By default, it will start jupyter.
You must give it the following environment variables:

```bash
LOCAL_HOME="/home/bernhard" \
LOCAL_CODE="/home/bernhard/Swiss2Text" \
PORT="8888" \
bash training/scripts/run.sh
```

The `LOCAL_HOME` is needed for the presistence of Jupyter settings. Generally, you don't actually load the models here, so the model cache (`/home/user/.cache/torch/NeMo`) is not loaded. Same with the data directory, it is not mounted in this image.
