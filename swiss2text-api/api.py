#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = "1.0.0"

import logging
from http import HTTPStatus
from pathlib import Path
from typing import Optional
from uuid import uuid1 as uuid

import nemo.collections.asr.modules
import numpy as np
import torch
from nemo.collections.asr.models import EncDecClassificationModel
from quart import jsonify, make_response, Quart, request, url_for, send_from_directory
from quart.datastructures import FileStorage
from quart_cors import cors
from sox import Transformer
from werkzeug.utils import secure_filename

import kenlm_utils
from nemo_utils import load_model

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(name)s [%(levelname)s] - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    force=True,
)

log = logging.getLogger("api")

UPLOAD_FOLDER = Path("upload")
ALLOWED_EXTENSIONS = {"wav", "mp3", "ogg"}

MODEL_PATH = Path("models")
LM_PATH = Path("language_models")
LM_MODELS = {
    "CH": "all-CH.kenlm",
    "DE": "all-DE.kenlm",
}

MODELS = {"pretrained_german": "stt_en_quartznet15x5"}
MODELS.update({f"{f.stem}": f for f in MODEL_PATH.glob("*.nemo")})


class ModelCache:
    def __init__(self):
        self.models: dict[str, EncDecClassificationModel] = {}

    def __getitem__(self, key) -> EncDecClassificationModel:
        if key not in self.models:
            self.models[key] = load_model(MODELS[key])

        return self.models[key]


MODEL_CACHE = ModelCache()

__CORS_SETTINGS = {"allow_origin": "*"}

app = Quart(f"{__name__}_app")
app.config["UPLOAD_FOLDER"] = str(UPLOAD_FOLDER)
app = cors(app, **__CORS_SETTINGS)

UPLOAD_FOLDER.mkdir(parents=True, exist_ok=True)


def _is_allowed_file(path: str):
    return "." in path and path.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/models", methods=["GET"])
async def list_models():
    return await make_response(jsonify({"models": sorted(MODELS.keys())}))


@app.route("/transcribe/<model_name>", methods=["PUT", "POST"])
async def transcribe(model_name):
    r = await request.files
    args = await request.form
    raw_file: Optional[FileStorage] = r["audio"]
    error = None
    if raw_file is None:
        error = "file is none", HTTPStatus.BAD_REQUEST
    else:
        if raw_file.filename == "":
            error = "file is empty", HTTPStatus.BAD_REQUEST
        else:
            if not _is_allowed_file(raw_file.filename):
                error = "illegal file format", HTTPStatus.BAD_REQUEST
            else:
                filename = Path(secure_filename(raw_file.filename))
                log.debug("Received file to store at %s", filename)
                target = UPLOAD_FOLDER.joinpath(f"{uuid()}{filename.suffix}")
                log.debug("Will save at %s", target)
                await raw_file.save(target)
                log.debug("Saved as %s", target)
                if filename.suffix != ".wav":
                    log.info("It is not wav, will have to convert.")
                    to_convert = target
                    target = to_convert.with_suffix(".wav")
                    tfm = Transformer()
                    tfm.rate(samplerate=16_000)
                    tfm.channels(n_channels=1)
                    tfm.build(
                        input_filepath=str(to_convert), output_filepath=str(target)
                    )
                    log.info("Convert to %s", target)
                    to_convert.unlink()

    if model_name is None or len(model_name) == 0:
        error = "you have to give model", HTTPStatus.BAD_REQUEST
    elif model_name not in MODELS:
        error = "this model doesn't exist", HTTPStatus.BAD_REQUEST

    if error is not None:
        message, status = error
        return await make_response(jsonify({"error": message}), status)

    model = MODEL_CACHE[model_name]

    with torch.no_grad():
        if (lm := args.get("lm", None)) is not None:
            log.info("Will use language model.")
            transcription = apply_language_model(
                model, target, LM_PATH.joinpath(LM_MODELS[lm])
            )
        else:
            log.info("Won't use language model.")
            transcription = model.transcribe([str(target)])[0]

    return await make_response(
        jsonify(
            {
                "transcription": transcription.strip(),
                "media": url_for("download_file", name=target.name),
            }
        ),
        HTTPStatus.OK,
    )


@app.route("/uploads/<name>")
async def download_file(name):
    return await send_from_directory(app.config["UPLOAD_FOLDER"], name)


TOKEN_OFFSET = 100


def apply_language_model(
    model: EncDecClassificationModel, file_to_transcribe: Path, lm_path: Path
) -> str:
    logits = model.transcribe([str(file_to_transcribe)], logprobs=True)[0]

    probs = kenlm_utils.softmax(logits)

    encoding_level = kenlm_utils.SUPPORTED_MODELS.get(type(model).__name__, None)
    if encoding_level is None:
        log.warning(f"This model type is not supported. Will use char encoding.")
        encoding_level = "char"

    vocab = model.decoder.vocabulary
    ids_to_text_func = None
    if encoding_level == "subword":
        vocab = [chr(idx + TOKEN_OFFSET) for idx in range(len(vocab))]
        ids_to_text_func = model.tokenizer.ids_to_text

    params = {"beam_width": 16, "beam_alpha": 2.0, "beam_beta": 1.5}

    transcription = run_beam_search(
        probs=probs,
        vocab=vocab,
        ids_to_text_func=ids_to_text_func,
        lm_path=lm_path,
        beam_width=16,
        beam_alpha=2.0,
        beam_beta=1.5,
    )

    return transcription


def run_beam_search(
    probs, vocab, ids_to_text_func, lm_path, beam_width, beam_alpha, beam_beta
):
    beam_search_lm = nemo.collections.asr.modules.BeamSearchDecoderWithLM(
        vocab=vocab,
        beam_width=beam_width,
        alpha=beam_alpha,
        beta=beam_beta,
        lm_path=str(lm_path),
        num_cpus=1,
        input_tensor=False,
    )

    beams = beam_search_lm.forward(
        log_probs=np.expand_dims(probs, axis=0), log_probs_length=None
    )[0]

    if ids_to_text_func is None:
        pred_text = beams[0][1]
    else:
        pred_text = ids_to_text_func([ord(c) - TOKEN_OFFSET for c in beams[0][1]])

    return pred_text
