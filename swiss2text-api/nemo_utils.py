from pathlib import Path
from typing import Union

import torch
import nemo.collections.asr as nemo_asr
from nemo.utils import model_utils

SAMPLE_RATE = 16_000


def list_models(path: Path) -> list[Path]:
    return list(path.glob("*.nemo"))


def load_model(model_name: Union[str, Path]) -> nemo_asr.models.ASRModel:
    if isinstance(model_name, Path):
        model_name = str(model_name)

    if model_name.endswith(".nemo"):
        load_fn = nemo_asr.models.ASRModel.restore_from
    else:
        load_fn = nemo_asr.models.ASRModel.from_pretrained
    cfg = load_fn(model_name, return_config=True)
    cfg.train_ds, cfg.validation_ds, cfg.test_ds = None, None, None

    model = load_fn(
        model_name, override_config_path=cfg, map_location=torch.device("cpu")
    )
    model.freeze()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    return model
