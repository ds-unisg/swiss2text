# Arguments
#
REPO="ics-unisg"
VERSION="1.0.0"
REMOTE_HOST="lasagne"

.PHONY: all api web

all: api web


api: swiss2text-api/Dockerfile
	cd swiss2text-api; \
	docker build \
  	-t ${REPO}/swiss2text-api:${VERSION} \
		.

web: swiss2text-web/Dockerfile
	cd swiss2text-web; \
	npm run build && \
	docker build \
		-t ${REPO}/swiss2text-web:${VERSION} \
		.

upload_web: web
	docker save "${REPO}/swiss2text-web:${VERSION}" | pv | zstd | ssh "${REMOTE_HOST}" 'zstdcat | docker load'

upload_api: api
	docker save "${REPO}/swiss2text-api:${VERSION}" | pv | zstd | ssh "${REMOTE_HOST}" 'zstdcat | docker load'

deploy:
	ssh "${REMOTE_HOST}" bash -c "\
		cd /home/bernhard/swiss2text \
		&& docker-compose down \
		&& eval('export UID=\$(id -u)') VERSION=${VERSION} docker-compose up -d"

